== Parsed Logical Plan ==
'Aggregate ['year, 'month, 'hotel_id], ['year, 'month, 'hotel_id, 'sum('visitors_count) AS visitors_count#1009]
+- 'UnresolvedRelation [expedia], [], false

== Analyzed Logical Plan ==
year: int, month: int, hotel_id: bigint, visitors_count: bigint
Aggregate [year#902, month#925, hotel_id#816L], [year#902, month#925, hotel_id#816L, sum(visitors_count#950) AS visitors_count#1009L]
+- SubqueryAlias expedia
   +- View (`expedia`, [id#797L,date_time#798,site_name#799,posa_continent#800,user_location_country#801,user_location_region#802,user_location_city#803,orig_destination_distance#804,user_id#805,is_mobile#806,is_package#807,channel#808,srch_ci#837,srch_co#858,srch_adults_cnt#811,srch_children_cnt#812,srch_rm_cnt#813,srch_destination_id#814,srch_destination_type_id#815,hotel_id#816L,srch_date#880,year#902,month#925,visitors_count#950])
      +- Project [id#797L, date_time#798, site_name#799, posa_continent#800, user_location_country#801, user_location_region#802, user_location_city#803, orig_destination_distance#804, user_id#805, is_mobile#806, is_package#807, channel#808, srch_ci#837, srch_co#858, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, srch_destination_id#814, srch_destination_type_id#815, hotel_id#816L, srch_date#880, year#902, month#925, visits_udf(srch_children_cnt#812, srch_adults_cnt#811, srch_rm_cnt#813) AS visitors_count#950]
         +- Project [id#797L, date_time#798, site_name#799, posa_continent#800, user_location_country#801, user_location_region#802, user_location_city#803, orig_destination_distance#804, user_id#805, is_mobile#806, is_package#807, channel#808, srch_ci#837, srch_co#858, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, srch_destination_id#814, srch_destination_type_id#815, hotel_id#816L, srch_date#880, year#902, month(srch_date#880) AS month#925]
            +- Project [id#797L, date_time#798, site_name#799, posa_continent#800, user_location_country#801, user_location_region#802, user_location_city#803, orig_destination_distance#804, user_id#805, is_mobile#806, is_package#807, channel#808, srch_ci#837, srch_co#858, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, srch_destination_id#814, srch_destination_type_id#815, hotel_id#816L, srch_date#880, year(srch_date#880) AS year#902]
               +- Project [id#797L, date_time#798, site_name#799, posa_continent#800, user_location_country#801, user_location_region#802, user_location_city#803, orig_destination_distance#804, user_id#805, is_mobile#806, is_package#807, channel#808, srch_ci#837, srch_co#858, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, srch_destination_id#814, srch_destination_type_id#815, hotel_id#816L, srch_date#880]
                  +- Generate explode(sequence(srch_ci#837, srch_co#858, Some(INTERVAL '1' MONTH), Some(Europe/Moscow))), false, [srch_date#880]
                     +- Filter (datediff(srch_co#858, srch_ci#837) >= 0)
                        +- Filter isnotnull(srch_co#858)
                           +- Filter isnotnull(srch_ci#837)
                              +- Project [id#797L, date_time#798, site_name#799, posa_continent#800, user_location_country#801, user_location_region#802, user_location_city#803, orig_destination_distance#804, user_id#805, is_mobile#806, is_package#807, channel#808, srch_ci#837, cast(srch_co#810 as date) AS srch_co#858, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, srch_destination_id#814, srch_destination_type_id#815, hotel_id#816L]
                                 +- Project [id#797L, date_time#798, site_name#799, posa_continent#800, user_location_country#801, user_location_region#802, user_location_city#803, orig_destination_distance#804, user_id#805, is_mobile#806, is_package#807, channel#808, cast(srch_ci#809 as date) AS srch_ci#837, srch_co#810, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, srch_destination_id#814, srch_destination_type_id#815, hotel_id#816L]
                                    +- Relation [id#797L,date_time#798,site_name#799,posa_continent#800,user_location_country#801,user_location_region#802,user_location_city#803,orig_destination_distance#804,user_id#805,is_mobile#806,is_package#807,channel#808,srch_ci#809,srch_co#810,srch_adults_cnt#811,srch_children_cnt#812,srch_rm_cnt#813,srch_destination_id#814,srch_destination_type_id#815,hotel_id#816L] avro

== Optimized Logical Plan ==
Aggregate [year#902, month#925, hotel_id#816L], [year#902, month#925, hotel_id#816L, sum(visitors_count#950) AS visitors_count#1009L]
+- Project [hotel_id#816L, year(srch_date#880) AS year#902, month(srch_date#880) AS month#925, pythonUDF0#1043 AS visitors_count#950]
   +- BatchEvalPython [visits_udf(srch_children_cnt#812, srch_adults_cnt#811, srch_rm_cnt#813)], [pythonUDF0#1043]
      +- Generate explode(sequence(srch_ci#837, srch_co#858, Some(INTERVAL '1' MONTH), Some(Europe/Moscow))), [0, 1], false, [srch_date#880]
         +- Project [cast(srch_ci#809 as date) AS srch_ci#837, cast(srch_co#810 as date) AS srch_co#858, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, hotel_id#816L]
            +- Filter ((isnotnull(srch_ci#809) AND isnotnull(srch_co#810)) AND (isnotnull(cast(srch_ci#809 as date)) AND (isnotnull(cast(srch_co#810 as date)) AND (datediff(cast(srch_co#810 as date), cast(srch_ci#809 as date)) >= 0))))
               +- Relation [id#797L,date_time#798,site_name#799,posa_continent#800,user_location_country#801,user_location_region#802,user_location_city#803,orig_destination_distance#804,user_id#805,is_mobile#806,is_package#807,channel#808,srch_ci#809,srch_co#810,srch_adults_cnt#811,srch_children_cnt#812,srch_rm_cnt#813,srch_destination_id#814,srch_destination_type_id#815,hotel_id#816L] avro

== Physical Plan ==
AdaptiveSparkPlan isFinalPlan=false
+- HashAggregate(keys=[year#902, month#925, hotel_id#816L], functions=[sum(visitors_count#950)], output=[year#902, month#925, hotel_id#816L, visitors_count#1009L])
   +- Exchange hashpartitioning(year#902, month#925, hotel_id#816L, 200), ENSURE_REQUIREMENTS, [id=#5223]
      +- HashAggregate(keys=[year#902, month#925, hotel_id#816L], functions=[partial_sum(visitors_count#950)], output=[year#902, month#925, hotel_id#816L, sum#1029L])
         +- Project [hotel_id#816L, year(srch_date#880) AS year#902, month(srch_date#880) AS month#925, pythonUDF0#1043 AS visitors_count#950]
            +- BatchEvalPython [visits_udf(srch_children_cnt#812, srch_adults_cnt#811, srch_rm_cnt#813)], [pythonUDF0#1043]
               +- Generate explode(sequence(srch_ci#837, srch_co#858, Some(INTERVAL '1' MONTH), Some(Europe/Moscow))), [srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, hotel_id#816L], false, [srch_date#880]
                  +- Project [cast(srch_ci#809 as date) AS srch_ci#837, cast(srch_co#810 as date) AS srch_co#858, srch_adults_cnt#811, srch_children_cnt#812, srch_rm_cnt#813, hotel_id#816L]
                     +- Filter ((((isnotnull(srch_ci#809) AND isnotnull(srch_co#810)) AND isnotnull(cast(srch_ci#809 as date))) AND isnotnull(cast(srch_co#810 as date))) AND (datediff(cast(srch_co#810 as date), cast(srch_ci#809 as date)) >= 0))
                        +- FileScan avro [srch_ci#809,srch_co#810,srch_adults_cnt#811,srch_children_cnt#812,srch_rm_cnt#813,hotel_id#816L] Batched: false, DataFilters: [isnotnull(srch_ci#809), isnotnull(srch_co#810), isnotnull(cast(srch_ci#809 as date)), isnotnull(..., Format: Avro, Location: InMemoryFileIndex(1 paths)[abfss://m07sparksql@bd201stacc.dfs.core.windows.net/expedia], PartitionFilters: [], PushedFilters: [IsNotNull(srch_ci), IsNotNull(srch_co)], ReadSchema: struct<srch_ci:string,srch_co:string,srch_adults_cnt:int,srch_children_cnt:int,srch_rm_cnt:int,ho...
