**Instruction for use**:

Install virtual env in `venv/` directory

Fill in `venv/bin/activate` you own env-variables' values from `.env.example`

Activate virtual env and your env-variables by `source venv/bin/activate`

Install requirements via `pip install -r requirements.txt`

To make the script-file (with **spark-submit** command) executable:
`sudo chmod +x ./scripts/local_run_submit.sh`

To run job locally:
`./scripts/local_run_submit.sh`

To run interactive pyspark-shell:
`sudo chmod +x ./scripts/local_run_it_shell.sh`
`./scripts/local_run_it_shell.sh`


**Screenshots are provided in repo's "screenshots" directory and below.**


**1 task**

_Top 10 hotels with max absolute temperature difference by month_

![](screenshots/1_task/1.final_result.png)

Reduced data (head-20) is stored locally (Azure account is forbidden for Russia):

![](screenshots/1_task/2.saved_data(shortened).png)


**2 task**

_Top 10 busy (e.g., with the biggest visits count) hotels for each month.
If visit dates refer to several months, it should be counted for all affected months._


Calculating visitors count for every month-hotel couple (month - with year).
Visitors count calculated as follows:
_(srch_children_cnt + srch_adults_cnt) * srch_rm_cnt_

![](screenshots/2_task/1.calc_visits_after_grouping.png)


Final results. Top 10 busy hotels for each month:

![](screenshots/2_task/2.final_result.png)


Reduced data (head-20) is stored locally:

![](screenshots/2_task/3.saved_data(shortened).png)


**3 task**

_For visits with extended stay (more than 7 days) calculate
weather trend (the day temperature difference between last and first day of stay)
and average temperature during stay._ 


Calculated first and last day for each hotel visit, and average temperature during stay:

![](screenshots/3_task/1.first-last-days.png)

Calculated first day's temperature for each hotel visit:

![](screenshots/3_task/2.first-day-tmpr.png)

Calculated last day's temperature for each hotel visit:

![](screenshots/3_task/3.last-day-tmpr.png)

Final result. Calculated day temperature difference between last and first day of stay and average temperature during stay:

![](screenshots/3_task/4.final-result.png)

Reduced data (head-20) is stored locally:

![](screenshots/3_task/5.saved-data(shortened).png)
