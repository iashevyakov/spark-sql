from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.functions import col, explode, expr, year, month, udf, datediff
from pyspark.sql.types import IntegerType


def clean_expedia_data(expedia_df: DataFrame) -> DataFrame:
    """ cast string-typed dates to date-type, filter out null-dates-rows, check for correct date range """
    expedia_df = expedia_df.withColumn("srch_ci", col("srch_ci").cast("date")).withColumn("srch_co", col("srch_co").cast("date"))
    expedia_df = expedia_df.filter(col("srch_ci").isNotNull()).filter(col("srch_co").isNotNull()).filter(datediff(col("srch_co"), col("srch_ci")) >= 0)
    return expedia_df


# cast string-typed date to date-type
def hotel_weather_date_cast(hotel_weather_df: DataFrame) -> DataFrame:
    return hotel_weather_df.withColumn("wthr_date", col("wthr_date").cast("date"))


# user-defined func for calculating visitors per booking-record
@udf(returnType=IntegerType())
def visits_udf(children_cnt: int, adult_cnt: int, room_cnt: int) -> int:
    return (children_cnt + adult_cnt) * room_cnt


def top_hotels_by_month_tmpr_diff(hotels_df: DataFrame, spark: SparkSession, hotels_limit: int = 10) -> DataFrame:

    """ Top 10 hotels with max absolute temperature difference by month """

    hotels_df.createOrReplaceTempView("hotel_weather")
    # grouping by month and hotel and taking max absolute temperature difference for each group
    hotels_tmpr_query = f"""
    SELECT year, month, id, name, max(avg_tmpr_c)-min(avg_tmpr_c) as abs_tmpr_diff 
    FROM hotel_weather 
    GROUP BY year, month, id, name 
    ORDER BY abs_tmpr_diff DESC 
    LIMIT {hotels_limit}
    """
    return spark.sql(hotels_tmpr_query)


def top_busy_hotels(expedia_df: DataFrame, spark: SparkSession, hotels_limit: int = 10) -> DataFrame:

    """ Top 10 busy (e.g., with the biggest visits count) hotels for each month.
    If visit dates refer to several months, it should be counted for all affected months. """

    # adding column "srch_date" by sequence of dates "srch_ci", "srch_co" with interval=1m
    expedia_df = expedia_df.withColumn(
        "srch_date", explode(expr("sequence(srch_ci, srch_co, interval 1 month)"))
    )
    # adding columns "year" and "month", whose values were parsed from "srch_date" column
    expedia_df = expedia_df.withColumn("year", year("srch_date")).withColumn("month", month("srch_date"))
    # adding column "visitors_count", values calculated by (children + adult) * room_count
    expedia_df = expedia_df.withColumn("visitors_count", visits_udf(col("srch_children_cnt"),
                                                                    col("srch_adults_cnt"), col("srch_rm_cnt")))

    expedia_df.createOrReplaceTempView("expedia")

    # grouping by month and hotel, calculating total visitors count for month-hotel couple
    busy_hotels_query = """
    SELECT year, month, hotel_id, sum(visitors_count) as visitors_count
    FROM expedia
    GROUP BY year, month, hotel_id
    """

    expedia_df = spark.sql(busy_hotels_query)
    expedia_df.createOrReplaceTempView("expedia_grouped_view")

    # calculating top 10 hotels for each month by visitors count
    busy_top_hotels_query = f"""
    SELECT year, month, hotel_id, visitors_count
    FROM (
    SELECT *, row_number() over (PARTITION BY year, month ORDER BY visitors_count DESC) as row_num
    FROM expedia_grouped_view
    )
    WHERE row_num <={hotels_limit}
    ORDER BY year, month, visitors_count DESC
    """

    busy_hotels_df = spark.sql(busy_top_hotels_query)
    return busy_hotels_df


def tmpr_trend_for_long_visits(
    hotel_weather_df: DataFrame,
    expedia_df: DataFrame,
    spark: SparkSession
) -> DataFrame:

    """ For visits with extended stay (more than 7 days) calculate
    weather trend (the day temperature difference between last and first day of stay)
    and average temperature during stay. """

    # considering only long visits (more than 7 days)
    expedia_df = expedia_df.filter(datediff(col("srch_co"), col("srch_ci")) > 7)
    expedia_df.createOrReplaceTempView("expedia")
    hotel_weather_df.createOrReplaceTempView("hotel_weather")

    # calculating first and last day for each hotel visit, and average temperature during stay:
    query = """
    SELECT expedia.id as exp_id, hotel_weather.id as hw_id,
    min(hotel_weather.wthr_date) as first_day, max(hotel_weather.wthr_date) as last_day, avg(avg_tmpr_c) as avg_tmpr
    FROM expedia 
    JOIN hotel_weather
    ON expedia.hotel_id = hotel_weather.id and hotel_weather.wthr_date BETWEEN expedia.srch_ci and expedia.srch_co
    GROUP BY expedia.id, hotel_weather.id
    """
    exp_hw_first_last_days_df = spark.sql(query)
    exp_hw_first_last_days_df.createOrReplaceTempView("first_last_days")

    # calculating first day's temperature for each hotel visit
    query = """
    SELECT exp.id as exp_id, hw.id as hw_id, hw.wthr_date as %s, hw.avg_tmpr_c as %s, fld.avg_tmpr
    FROM expedia as exp 
    JOIN hotel_weather as hw
    ON exp.hotel_id = hw.id and hw.wthr_date BETWEEN exp.srch_ci and exp.srch_co
    JOIN 
    first_last_days as fld
    ON exp.id = fld.exp_id and hw.id = fld.hw_id and hw.wthr_date = fld.%s
    """
    first_day_tmpr_query = query % ("first_day", "first_day_tmpr", "first_day")
    first_day_tmpr = spark.sql(first_day_tmpr_query)
    first_day_tmpr.createOrReplaceTempView("first_day_tmpr_view")

    # calculating last day's temperature for each hotel visit
    last_day_tmpr_query = query % ("last_day", "last_day_tmpr", "last_day")
    last_day_tmpr = spark.sql(last_day_tmpr_query)
    last_day_tmpr.createOrReplaceTempView("last_day_tmpr_view")

    # calculating day temperature difference between last and first day of stay
    # and average temperature during stay
    query = """
    SELECT fdt.exp_id as booking_id, fdt.hw_id as hotel_id, fdt.first_day, fdt.first_day_tmpr,
    ldt.last_day, ldt.last_day_tmpr, ldt.last_day_tmpr - fdt.first_day_tmpr as diff_tmpr, ldt.avg_tmpr
    FROM first_day_tmpr_view as fdt 
    JOIN 
    last_day_tmpr_view as ldt
    ON fdt.exp_id = ldt.exp_id and fdt.hw_id = ldt.hw_id
    """

    first_last_days_tmpr = spark.sql(query)

    return first_last_days_tmpr
