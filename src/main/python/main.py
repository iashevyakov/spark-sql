import os
from pyspark import SparkConf
from pyspark.shell import sqlContext
from pyspark.sql import SparkSession
from src.main.python.transform import hotel_weather_date_cast, top_hotels_by_month_tmpr_diff, clean_expedia_data, \
    top_busy_hotels, tmpr_trend_for_long_visits


# setup spark config for access to azure storage
def setup_spark_config() -> SparkConf:
    conf = SparkConf()
    conf.set("fs.azure.account.auth.type.bd201stacc.dfs.core.windows.net", os.environ.get("AUTH_TYPE"))
    conf.set("fs.azure.account.oauth.provider.type.bd201stacc.dfs.core.windows.net", os.environ.get("PROVIDER_TYPE"))
    conf.set("fs.azure.account.oauth2.client.id.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_ID"))
    conf.set("fs.azure.account.oauth2.client.secret.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_SECRET"))
    conf.set("fs.azure.account.oauth2.client.endpoint.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_ENDPOINT"))
    return conf


if __name__ == "main":
    # setup config for spark session
    conf = setup_spark_config()

    # spark session's initialization
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # reading dataframe with hotels and weather
    hotel_weather_df = spark.read.parquet(os.environ.get("HOTEL_WEATHER_PATH"))
    # casting string-typed column "wthr_date" to date-type
    hotel_weather_df = hotel_weather_date_cast(hotel_weather_df)

    # calculation top hotels with biggest tmpr diff by month (1 task)
    top_hotels_by_tmpr_diff = top_hotels_by_month_tmpr_diff(hotel_weather_df, spark)
    # explaining logical and physical execution plans
    top_hotels_by_tmpr_diff.explain(extended=True)
    # saving shortened version locally (because Azure account is not available now for Russia)
    top_hotels_by_tmpr_diff_shortened = sqlContext.createDataframe(top_hotels_by_tmpr_diff.head(20),
                                                                   top_hotels_by_tmpr_diff.schema)
    top_hotels_by_tmpr_diff_shortened.write.mode("overwrite").format("parquet").save(os.environ.get("PATH_MONTH_DIFF"))

    # reading dataframe with expedia data
    expedia_df = spark.read.format("avro").load(os.environ.get("EXPEDIA_PATH"))
    # casting string-typed dates to date-type, filter out null-dates-rows, checking for correct date-range
    expedia_df = clean_expedia_data(expedia_df)

    # calculation top 10 busy hotels for each month (2 task)
    top_n_busy_hotels = top_busy_hotels(expedia_df, spark)
    # explaining logical and physical execution plans
    top_n_busy_hotels.explain(extended=True)
    # saving shortened version locally (because Azure account is not available now for Russia)
    top_busy_hotels_shortened = sqlContext.createDataframe(top_n_busy_hotels.head(20), top_n_busy_hotels.schema)
    top_busy_hotels_shortened.write.mode("overwrite").format("parquet").save(os.environ.get("PATH_TOP_BUSY"))

    # calculation trend (first-day-tmpr, last-day-tmpr, avg-tmpr during stay) for long visits (3 task)
    trend_for_long_visits = tmpr_trend_for_long_visits(hotel_weather_df, expedia_df, spark)
    # explaining logical and physical execution plans
    trend_for_long_visits.explain(extended=True)
    # saving shortened version locally (because Azure account is not available now for Russia)
    trend_for_long_visits_shortened = sqlContext.createDataframe(trend_for_long_visits.head(20),
                                                                 trend_for_long_visits.schema)
    trend_for_long_visits_shortened.write.mode("overwrite").format("parquet").save(os.environ.get("PATH_TMPR_TREND"))



