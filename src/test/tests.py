import datetime
import unittest

from pyspark.sql import SparkSession
from pyspark.sql.functions import collect_list
from pyspark.sql.types import StructField, StructType, DoubleType, IntegerType, StringType, DateType

from src.main.python.transform import top_hotels_by_month_tmpr_diff, top_busy_hotels, tmpr_trend_for_long_visits


class SparkSQLTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # preparing session for spark
        cls.spark = (SparkSession
                     .builder
                     .master("local[*]")
                     .appName("pyspark-sql-tests")
                     .getOrCreate())

    def testTopHotelsByMonthTmprDiff(self):
        # preparing schema and data for input dataframe
        input_schema = StructType([
            StructField('year', IntegerType()),
            StructField('month', IntegerType()),
            StructField('id', IntegerType()),
            StructField('name', StringType()),
            StructField('avg_tmpr_c', DoubleType())
        ])
        input_data = [
            (2019, 8, 1, "Hotel 1", 28.0),
            (2019, 8, 1, "Hotel 1", -2.0),
            (2019, 7, 1, "Hotel 1", 10.0),
            (2020, 9, 2, "Hotel 2", 30.0),
            (2020, 9, 2, "Hotel 2", 10.0),
            (2020, 7, 3, "Hotel 3", 20.0),
            (2020, 7, 3, "Hotel 3", -5.0),
        ]
        input_df = self.spark.createDataFrame(data=input_data, schema=input_schema)

        # calling function in order to test her return value further
        df = top_hotels_by_month_tmpr_diff(hotels_df=input_df, spark=self.spark)

        # checking columns' ("abs_tmpr_diff", "name") values
        self.assertEqual(df.select(collect_list('abs_tmpr_diff')).first()[0], [30.0, 25.0, 20.0, 0.0])
        self.assertEqual(df.select(collect_list('name')).first()[0], ["Hotel 1", "Hotel 3", "Hotel 2", "Hotel 1"])

    def testTopBusyHotels(self):
        # preparing schema and data for input dataframe
        input_schema = StructType([
            StructField('srch_ci', DateType()),
            StructField('srch_co', DateType()),
            StructField('hotel_id', IntegerType()),
            StructField('srch_children_cnt', IntegerType()),
            StructField('srch_adults_cnt', IntegerType()),
            StructField('srch_rm_cnt', IntegerType()),
        ])
        input_data = [
            (datetime.date(2019, 8, 10), datetime.date(2019, 8, 10), 1, 10, 0, 1),
            (datetime.date(2019, 8, 10), datetime.date(2019, 8, 10), 1, 5, 0, 1),
            (datetime.date(2019, 8, 10), datetime.date(2019, 8, 10), 2, 15, 0, 1),
            (datetime.date(2019, 8, 10), datetime.date(2019, 8, 10), 2, 5, 0, 1),
            (datetime.date(2019, 8, 10), datetime.date(2019, 8, 10), 3, 20, 0, 1),
            (datetime.date(2019, 8, 10), datetime.date(2019, 8, 10), 3, 5, 0, 1),
            (datetime.date(2019, 9, 10), datetime.date(2019, 9, 10), 3, 10, 0, 1),
            (datetime.date(2019, 9, 10), datetime.date(2019, 9, 10), 3, 5, 0, 1),
            (datetime.date(2019, 9, 10), datetime.date(2019, 9, 10), 2, 15, 0, 1),
            (datetime.date(2019, 9, 10), datetime.date(2019, 9, 10), 2, 5, 0, 1),
            (datetime.date(2019, 9, 10), datetime.date(2019, 9, 10), 1, 20, 0, 1),
            (datetime.date(2019, 9, 10), datetime.date(2019, 9, 10), 1, 5, 0, 1),
        ]
        input_df = self.spark.createDataFrame(data=input_data, schema=input_schema)

        # calling function in order to test her return value further
        df = top_busy_hotels(expedia_df=input_df, spark=self.spark)

        # checking columns' ("month", "hotel_id", "visitors_count") values
        self.assertEqual(df.select(collect_list('month')).first()[0], [8] * 3 + [9] * 3)
        self.assertEqual(df.select(collect_list('hotel_id')).first()[0], [3, 2, 1, 1, 2, 3])
        self.assertEqual(df.select(collect_list('visitors_count')).first()[0], [25, 20, 15, 25, 20, 15])

    def testTmprTrendForLongVisits(self):
        # preparing schema and data for input dataframe
        input_schema_expedia = StructType([
            StructField("id", IntegerType()),
            StructField("hotel_id", IntegerType()),
            StructField("srch_ci", DateType()),
            StructField("srch_co", DateType()),
        ])
        input_schema_hotel_weather = StructType([
            StructField("id", IntegerType()),
            StructField("wthr_date", DateType()),
            StructField("avg_tmpr_c", DoubleType())
        ])

        input_data_expedia = [
            (1, 1, datetime.date(year=2019, month=8, day=10), datetime.date(year=2019, month=8, day=18)),
            (2, 1, datetime.date(year=2019, month=8, day=10), datetime.date(year=2019, month=8, day=20))
        ]
        input_data_hotel_weather = [
            (1, datetime.date(2019, 8, 10), 1.0),
            (1, datetime.date(2019, 8, 11), 3.0),
            (1, datetime.date(2019, 8, 12), 4.0),
            (1, datetime.date(2019, 8, 13), 5.0),
            (1, datetime.date(2019, 8, 14), 2.0),
            (1, datetime.date(2019, 8, 15), 2.0),
            (1, datetime.date(2019, 8, 16), 2.0),
            (1, datetime.date(2019, 8, 17), 2.0),
            (1, datetime.date(2019, 8, 18), 6.0),
            (1, datetime.date(2019, 8, 19), 3.0),
            (1, datetime.date(2019, 8, 20), 3.0),
        ]
        input_df_expedia = self.spark.createDataFrame(input_data_expedia, input_schema_expedia)
        input_df_hotel_weather = self.spark.createDataFrame(input_data_hotel_weather, input_schema_hotel_weather)

        # calling function in order to test her return value further
        df = tmpr_trend_for_long_visits(input_df_hotel_weather, input_df_expedia, spark=self.spark)

        # checking columns' ("avg_tmpr", "diff_tmpr") values
        self.assertEqual(df.select(collect_list('avg_tmpr')).first()[0], [3.0, 3.0])
        self.assertEqual(df.select(collect_list('diff_tmpr')).first()[0], [5.0, 2.0])

    @classmethod
    def tearDownClass(cls):
        # stopping spark's session
        cls.spark.stop()